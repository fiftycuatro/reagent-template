(ns leiningen.new.fiftycuatro-reagent
  (:use 
    [leiningen.new.templates :only [renderer name-to-path sanitize-ns ->files]]
    [clojure.java.shell :only [sh]])
  (:require
    [clojure.java.io :as io]
    [clojure.string :as str]))

(def render (renderer "fiftycuatro_reagent"))

(defn binary
  [file]
  (io/input-stream (io/resource (clojure.string/join "/" ["leiningen" "new" "fiftycuatro_reagent" file]))))

(defn fiftycuatro-reagent 
  [name]
  (let [short-name (-> (str/split name #"\.") reverse first) 
        data {:name short-name
              :ns-name (sanitize-ns name)
              :sanitized (name-to-path name)}]
    (->files data 
             ["src/clj/{{sanitized}}/main.clj"                     (render "src/clj/main.clj" data)]
             ["src/clj/{{sanitized}}/core.clj"                     (render "src/clj/core.clj" data)]
             ["src/cljs/{{sanitized}}/core.cljs"                   (render "src/cljs/core.cljs" data)]
             ["dev/clj/user.clj"                                   (render "dev/clj/user.clj" data)]
             ["test/clj/{{sanitized}}/core_test.clj"               (render "test/clj/core_test.clj" data)]
             ["project.clj"                                        (render "project.clj" data)]
             ["resources/public/css/fontawesome-all.css"           (binary "resources/public/css/fontawesome-all.css")]
             ["resources/public/webfonts/fa-brands-400.eot"        (binary "resources/public/webfonts/fa-brands-400.eot")]
             ["resources/public/webfonts/fa-brands-400.svg"        (binary "resources/public/webfonts/fa-brands-400.svg")]
             ["resources/public/webfonts/fa-brands-400.ttf"        (binary "resources/public/webfonts/fa-brands-400.ttf")]
             ["resources/public/webfonts/fa-brands-400.woff"       (binary "resources/public/webfonts/fa-brands-400.woff")]
             ["resources/public/webfonts/fa-brands-400.woff2"      (binary "resources/public/webfonts/fa-brands-400.woff2")]
             ["resources/public/webfonts/fa-regular-400.eot"       (binary "resources/public/webfonts/fa-regular-400.eot")]
             ["resources/public/webfonts/fa-regular-400.svg"       (binary "resources/public/webfonts/fa-regular-400.svg")]
             ["resources/public/webfonts/fa-regular-400.ttf"       (binary "resources/public/webfonts/fa-regular-400.ttf")]
             ["resources/public/webfonts/fa-regular-400.woff"      (binary "resources/public/webfonts/fa-regular-400.woff")]
             ["resources/public/webfonts/fa-regular-400.woff2"     (binary "resources/public/webfonts/fa-regular-400.woff2")]
             ["resources/public/webfonts/fa-solid-900.eot"         (binary "resources/public/webfonts/fa-solid-900.eot")]
             ["resources/public/webfonts/fa-solid-900.svg"         (binary "resources/public/webfonts/fa-solid-900.svg")]
             ["resources/public/webfonts/fa-solid-900.ttf"         (binary "resources/public/webfonts/fa-solid-900.ttf")]
             ["resources/public/webfonts/fa-solid-900.woff"        (binary "resources/public/webfonts/fa-solid-900.woff")]
             ["resources/public/webfonts/fa-solid-900.woff2"       (binary "resources/public/webfonts/fa-solid-900.woff2")]
             ["resources/public/webfonts/lato_font.css"            (binary "resources/public/webfonts/lato_font.css")]
             ["resources/public/webfonts/latin_300.woff2"          (binary "resources/public/webfonts/latin_300.woff2")]
             ["resources/public/webfonts/latin_ext_300.woff2"      (binary "resources/public/webfonts/latin_ext_300.woff2")]
             ["resources/public/webfonts/latin_400.woff2"          (binary "resources/public/webfonts/latin_400.woff2")]
             ["resources/public/webfonts/latin_ext_400.woff2"      (binary "resources/public/webfonts/latin_ext_400.woff2")]
             ["resources/public/webfonts/latin_900.woff2"          (binary "resources/public/webfonts/latin_900.woff2")]
             ["resources/public/webfonts/latin_ext_900.woff2"      (binary "resources/public/webfonts/latin_ext_900.woff2")]
             ["resources/public/css/bulma.css"                     (binary "resources/public/css/bulma.css")]
             ["resources/public/css/style.css"                     (binary "resources/public/css/style.css")]
             ["resources/public/index.html"                        (render "resources/public/index.html" data)]
             ["resources/logback.xml"                              (binary "resources/logback.xml")]
             [".gitignore"                                         (binary ".gitignore")]
             ["CHANGELOG.md"                                       (binary "CHANGELOG.md")]
             ["LICENSE"                                            (binary "LICENSE")]
             ["README.md"                                          (render "README.md" data)]
             ["open-in-intellij.sh"                                (binary "open-in-intellij.sh")])
    (sh "chmod" "+x" (str short-name "/open-in-intellij.sh"))))
