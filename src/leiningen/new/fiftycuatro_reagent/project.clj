(defproject {{ns-name}} "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/mit-license.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [com.stuartsierra/component "0.3.2"]
                 [org.clojure/tools.cli "0.3.5"]
                 [org.clojure/tools.logging "0.4.0"]
                 [ch.qos.logback/logback-classic "1.1.7"] 

                 ;; HTTP server deps 
                 [compojure/compojure "1.5.1"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-jetty-adapter "1.6.3"]
                 [ring/ring-json "0.4.0"]
                
                 ;; Front end libs 
                 [reagent "0.8.0-alpha2"]]

  :source-paths ["src/clj"]
  :test-paths ["test/clj"]
  :plugins [[lein-cljsbuild "1.1.7"]
            [lein-figwheel "0.5.14"]]
  :clean-targets ^{:protect false} [:target-path "resources/public/js/app/"] 
  :profiles {:uberjar {:aot        :all
                       :prep-tasks ["compile" ["cljsbuild" "once" "min"]]
                       :main       {{ns-name}}.main}
             
             :dev {:source-paths ["src/clj" "dev/clj"]
                   :dependencies [[org.clojure/tools.namespace "0.2.11"]
                                  [com.cemerick/piggieback "0.2.1"]
                                  [figwheel-sidecar "0.5.14"]]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}
             :provided {:dependencies [[org.clojure/clojurescript "1.9.946"]]}}

  :figwheel {:open-file-command "./open-in-intellij.sh"}
  :cljsbuild {:builds [{:id "dev"
                        :figwheel {:on-jsload "{{ns-name}}.core/run"}
                        :source-paths ["src/cljs"]
                        :compiler {:main "{{ns-name}}.core"
                                   :optimizations :none
                                   :source-map true
                                   :source-map-timestamp true
                                   :asset-path "js/app/out"
                                   :output-dir "resources/public/js/app/out"
                                   :output-to  "resources/public/js/app/app.js"
                                   }}
                       {:id "min"
                        :source-paths ["src/cljs"]
                        :compiler {:main "{{ns-name}}.core"
                                   :optimizations :advanced
                                   :asset-path "js/app/out"
                                   :output-to  "resources/public/js/app/app.js"}}]}
  )
