(ns user
  (:require
    [com.stuartsierra.component :as component]
    [clojure.tools.namespace.repl :refer [refresh]]
    [figwheel-sidecar.system :as sys]
    [{{ns-name}}.core :as core]))

(def system nil)

(defn init
  []
  (alter-var-root #'system
                  (constantly
                    (component/system-map
                      :figwheel-system (sys/figwheel-system (sys/fetch-config))
                      :app-system (core/server {:port 8082})))))


(defn start
  []
  (alter-var-root #'system component/start))

(defn stop
  []
  (alter-var-root #'system
                  (fn [s] (when s (component/stop s)))))

(defn go
  []
  (init)
  (start)
  nil)

(defn reset
  []
  (stop)
  (refresh :after 'user/go))

(defn cljs-repl
  []
  (sys/cljs-repl (:figwheel-system system)))

