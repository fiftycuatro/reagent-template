(ns {{ns-name}}.core
  (:require 
    [com.stuartsierra.component :as component]
    [compojure.handler]
    [compojure.core :as compojure :refer [GET POST DELETE]]
    [compojure.route :refer [resources not-found]]
    [ring.util.response :refer [response redirect]]
    [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
    [ring.adapter.jetty :refer [run-jetty]]
    [clojure.tools.logging :as log]))

(def api-routes 
  (compojure/routes
    (GET "/about" [] (response {:version "0.0.1"}))
    (GET "/" [] (redirect "index.html"))
    (resources "/")
    (not-found {:status 404})))

(def app-handler
  (-> api-routes
      compojure.handler/api
      (wrap-json-body {:keywords? true})
      wrap-json-response))
                            
(defrecord Server 
  [port handler server]
  component/Lifecycle
  (start
    [{:keys [port] :as this}]
    (log/info "Starting on port " port)
    (let [server (run-jetty handler {:port port :join? false})]
      (assoc this :server server)))
  (stop
    [{:keys [server] :as this}]
    (log/info "Stopping")
    (.stop server)
    this))

(defn server
  [options]
  (map->Server (assoc options :handler app-handler)))
