(ns {{ns-name}}.main 
  (:require 
    [clojure.set :as set]
    [clojure.tools.cli :as cli]
    [clojure.tools.logging :as log]
    [com.stuartsierra.component :as component]
    [{{ns-name}}.core :as core])
  (:gen-class))

(def required-opts #{})

(defn required-error-msg
  [opts]
  (->> ["The following options are required:"
	""
	""
	(clojure.string/join \newline (set/difference required-opts opts))]
       (clojure.string/join \newline)))

(defn missing-required?
  [opts]
  (not-every? opts required-opts))

(defn error-msg
  [errors]
  (str "The following errors occured while parsing your command:\n\n"
       (clojure.string/join \newline errors)))

(defn exit
  [status msg]
  (println msg)
  (System/exit status))

(defn usage
  [options-summary]
  (->> ["{{name}}"
        ""
        "Usage: {{name}} [options]"
        ""
        "Options:"
        options-summary]
       (clojure.string/join \newline)))

(def cli-options
    [["-p" "--port PORT" "Server Port"
      :default 8082
      :parse-fn #(Integer/parseInt %)
      :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
     ["-h" "--help"]])

(def system (atom nil))

(defn -main
  [& args]
  (let [{:keys [options errors summary arguments]} (cli/parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (missing-required? options) (exit 0 (required-error-msg options))
      errors (exit 1 (error-msg errors)))
    :default (do
               (log/info (str "Starting System with options " options))
               (let [system (component/start (core/server options))]
                 ;; Shutdown server on exit
                 (.addShutdownHook (Runtime/getRuntime) (Thread. (fn [] (component/stop system))))))))

