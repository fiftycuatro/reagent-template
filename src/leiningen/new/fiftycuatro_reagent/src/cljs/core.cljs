(ns {{ns-name}}.core
  (:require
    [reagent.core :as reagent]))

(defn header
  []
  [:header
   [:i.fas.fa-home.fa-2x]
   [:span.title "<BRAND>"]])

(defn footer
  []
  [:footer
   {:style {:display :flex
            :justify-content :center}}
   [:p.thin "Copyright (c) <YEAR> <AUTHOR>"]])

(defn left-pane 
  []
  [:p "Left"])

(defn right-pane 
  []
  [:p "Right"])

(defn content
  []
   [:p "Content"])

(defn page-layout
  [{:keys [header content left right footder]}]
  [:div.layout-container
   [:div.border-bottom [header]]
   [:div.layout-main
    [:div.layout-content [content]]
    (when left
      [:div.layout-left.border-right [left]])
    (when right 
      [:div.layout-right.border-left [right]])]
   [:div.border-top [footer]]])
 
(defn page
  []
  (fn []
    [page-layout {:header header
                  :content content 
                  :left left-pane
                  :right right-pane
                  :footer footer}]))

(defn ^:export run []
  (reagent/render [#'page]
                  (js/document.getElementById "app")))
