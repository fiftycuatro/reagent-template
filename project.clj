(defproject fiftycuatro-reagent/lein-template "1.0-SNAPSHOT"
  :description "Template for a basic reagent setup"
  :url "https://bitbucket.org/fiftycuatro/reagent-template"
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/mit-license.html"}
  :deploy-repositories [["releases"  {:sign-releases false :url "https://clojars.org/repo"}]
                        ["snapshots" {:sign-releases false :url "https://clojars.org/repo"}]]
  :eval-in-leiningen true)
